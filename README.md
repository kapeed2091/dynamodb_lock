# Usage

## Type-1

Type-1 logic is dependent on client's time

### Acquire lock

```text
lock_client = DynamoDBLock(lock_db_name="KontestLockTable")

lock_status = lock_client.acquire_lock()  # returns True if lock acquired else False

Scenarios handled:

  1. Lock is acquired for first time (should return True)
  2. Lock active, another request for lock (should return False)
  3. Lock expired, another request for lock (should return True)
```

### Release lock

```text
lock_client = DynamoDBLock(lock_db_name="KontestLockTable")

lock_client.release_lock(lock_name='TEST', lock_duration_in_seconds=10)

Scenarios handled:

  1. There is an entry for given lock (delete the entry)
  2. There is no entry for given lock (boto delete -> graceful exit)
```

### Time
Both acquire_lock and release_lock not taking more than 0.3 seconds from local system (with cloud in Mumbai region)
 
 
## Type-2

Reference: https://aws.amazon.com/blogs/database/building-distributed-locks-with-the-dynamodb-lock-client/
Python client to the logic mentioned in above link

We have two implementations to this approach. Implementation-2 isn't 
