import boto3
import time
import uuid


class DynamoDBLock(object):

    def __init__(self, lock_db_name, region_name='ap-south-1'):
        self.lock_db_name = lock_db_name
        self.dynamodb_client = boto3.client('dynamodb',
                                            region_name=region_name)
        self.region_name = region_name

    def create_table(self):
        try:
            self.dynamodb_client.create_table(
                AttributeDefinitions=[
                    {
                        'AttributeName': 'name',
                        'AttributeType': 'S',
                    }
                ],
                KeySchema=[
                    {
                        'AttributeName': 'name',
                        'KeyType': 'HASH',
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5,
                },
                TableName=self.lock_db_name,
            )
        except self.dynamodb_client.exceptions.ResourceInUseException:
            pass

    def acquire_lock(self, lock_name, lock_duration_in_seconds):
        """

        :param lock_name:
        :param lock_duration_in_seconds:
        :return:

         State-1: Lock is acquired for first time
         State-2: Lock is active, request for another lock
         State-3: Lock is inactive, request for another lock

        """
        luid = str(uuid.uuid4())
        time_now = time.time()
        expires_at = time_now + lock_duration_in_seconds
        print 'time_now:', time_now
        print 'expires_at:', expires_at
        params = {
            "TableName": self.lock_db_name,
            "Item": {
                "name": {
                    "S": lock_name,
                },
                "expires_at": {
                    "N": str(expires_at)
                },
                "luid": {
                    "S": luid
                }
            },
            "ConditionExpression": "attribute_not_exists(expires_at) OR (attribute_exists(expires_at) AND expires_at < :now)",
            'ExpressionAttributeValues': {
                ":now": {"N": str(time_now)}
            }
        }

        try:
            self.dynamodb_client.put_item(**params)
            return True
        except Exception as e:
            print 'Exception:', e
            return False

    def release_lock(self, lock_name):
        params = {
            "TableName": self.lock_db_name,
            "Key": {
                "name": {
                    "S": lock_name,
                }
            }
        }
        try:
            self.dynamodb_client.delete_item(**params)
        except Exception as e:
            print 'Exception:', e
            pass
        return

    def reacquire_lock(self, lock_name, lock_duration_in_seconds, luid):
        time_now = time.time()
        print 'time_now:', time_now
        params = {
            "TableName": self.lock_db_name,
            "Key": {
                "name": {
                    "S": lock_name,
                }
            },
            "UpdateExpression": "SET expires_at = expires_at + :lock_duration",
            "ConditionExpression": "attribute_exists(expires_at) AND attribute_exists(luid) AND luid = :luid AND expires_at < :now",
            "ExpressionAttributeValues": {
                ":lock_duration": {"N": str(lock_duration_in_seconds)},
                ":now": {"N": str(time_now)},
                ":luid": {"S": luid}
            }
        }

        try:
            self.dynamodb_client.update_item(**params)
            return True
        except Exception as e:
            print 'Exception:', e
            return False
