class ReleaseLockOptions(object):
    def __init__(self, lock_item, delete_lock=None, best_effort=None,
                 data=None, request_metric_collector=None):
        self.lock_item = lock_item
        self.delete_lock = delete_lock
        self.best_effort = best_effort
        self.data = data
        self.request_metric_collector = request_metric_collector

    def with_delete_lock(self, delete_lock):
        self.delete_lock = delete_lock
        return self

    def with_best_effort(self, best_effort):
        self.best_effort = best_effort
        return self

    def with_data(self, data):
        self.data = data
        return self

    def with_request_metric_collector(self, request_metric_collector):
        self.request_metric_collector = request_metric_collector
        return self

    @classmethod
    def builder(cls, lock_item):
        return cls(lock_item=lock_item)

    def is_delete_lock(self):
        return self.delete_lock

    def is_best_effort(self):
        return self.best_effort

    def get_data(self):
        return self.data