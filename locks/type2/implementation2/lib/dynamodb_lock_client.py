import time


class DynamoDBLockClient(object):
    SK_PATH_EXPRESSION_VARIABLE = "#sk"
    PK_PATH_EXPRESSION_VARIABLE = "#pk"
    NEW_RVN_VALUE_EXPRESSION_VARIABLE = ":newRvn"
    LEASE_DURATION_PATH_VALUE_EXPRESSION_VARIABLE = "#ld"
    LEASE_DURATION_VALUE_EXPRESSION_VARIABLE = ":ld"
    RVN_PATH_EXPRESSION_VARIABLE = "#rvn"
    RVN_VALUE_EXPRESSION_VARIABLE = ":rvn"
    OWNER_NAME_PATH_EXPRESSION_VARIABLE = "#on"
    OWNER_NAME_VALUE_EXPRESSION_VARIABLE = ":on"
    DATA_PATH_EXPRESSION_VARIABLE = "#d"
    DATA_VALUE_EXPRESSION_VARIABLE = ":d"
    IS_RELEASED_PATH_EXPRESSION_VARIABLE = "#ir"
    IS_RELEASED_VALUE_EXPRESSION_VARIABLE = ":ir"

    ACQUIRE_LOCK_THAT_DOESNT_EXIST_OR_IS_RELEASED_CONDITION = \
        "attribute_not_exists(%s) OR (attribute_exists(%s) AND %s = %s)".format(
            PK_PATH_EXPRESSION_VARIABLE, PK_PATH_EXPRESSION_VARIABLE,
            IS_RELEASED_PATH_EXPRESSION_VARIABLE,
            IS_RELEASED_VALUE_EXPRESSION_VARIABLE)

    PK_EXISTS_AND_SK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION = \
        "attribute_exists(%s) AND attribute_exists(%s) AND %s = %s".format(
            PK_PATH_EXPRESSION_VARIABLE, SK_PATH_EXPRESSION_VARIABLE,
            RVN_PATH_EXPRESSION_VARIABLE, RVN_VALUE_EXPRESSION_VARIABLE)

    PK_EXISTS_AND_SK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION = \
        "%s AND %s = %s".format(
            PK_EXISTS_AND_SK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION,
            OWNER_NAME_PATH_EXPRESSION_VARIABLE,
            OWNER_NAME_VALUE_EXPRESSION_VARIABLE)

    PK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION = \
        "attribute_exists(%s) AND %s = %s".format(
            PK_PATH_EXPRESSION_VARIABLE, RVN_PATH_EXPRESSION_VARIABLE,
            RVN_VALUE_EXPRESSION_VARIABLE)

    PK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION = \
        "%s AND %s = %s".format(PK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION,
                                OWNER_NAME_PATH_EXPRESSION_VARIABLE,
                                OWNER_NAME_VALUE_EXPRESSION_VARIABLE)

    UPDATE_IS_RELEASED = "SET %s = %s".format(
        IS_RELEASED_PATH_EXPRESSION_VARIABLE,
        IS_RELEASED_VALUE_EXPRESSION_VARIABLE)
    UPDATE_IS_RELEASED_AND_DATA = "%s, %s = %s".format(
        UPDATE_IS_RELEASED, DATA_PATH_EXPRESSION_VARIABLE,
        DATA_VALUE_EXPRESSION_VARIABLE)
    UPDATE_LEASE_DURATION_AND_RVN = "SET %s = %s, %s = %s".format(
        LEASE_DURATION_PATH_VALUE_EXPRESSION_VARIABLE,
        LEASE_DURATION_VALUE_EXPRESSION_VARIABLE, RVN_PATH_EXPRESSION_VARIABLE,
        NEW_RVN_VALUE_EXPRESSION_VARIABLE)
    UPDATE_LEASE_DURATION_AND_RVN_AND_REMOVE_DATA = "%s REMOVE %s".format(
        UPDATE_LEASE_DURATION_AND_RVN, DATA_PATH_EXPRESSION_VARIABLE)
    UPDATE_LEASE_DURATION_AND_RVN_AND_DATA = "%s, %s = %s".format(
        UPDATE_LEASE_DURATION_AND_RVN, DATA_PATH_EXPRESSION_VARIABLE,
        DATA_VALUE_EXPRESSION_VARIABLE)

    DATA = "data"
    OWNER_NAME = "owner_name"
    LEASE_DURATION = "lease_duration"
    RECORD_VERSION_NUMBER = "record_version_number"
    IS_RELEASED = "is_released"
    IS_RELEASED_VALUE = "1"

    IS_RELEASED_ATTRIBUTE_VALUE = None

    DEFAULT_BUFFER = 1000

    def __init__(self, dynamodb_lock_client_options):
        self.dynamodb_lock_client_options = dynamodb_lock_client_options
        self.dynamodb_client = dynamodb_lock_client_options.get_dynamodb_client()
        self.table_name = dynamodb_lock_client_options.get_table_name()
        self.locks = {}  # TODO
        self.session_monitors = {}  # TODO
        self.owner_name = dynamodb_lock_client_options.get_owner_name()
        self.lease_duration = dynamodb_lock_client_options.get_lease_duration()
        self.heartbeat_period = dynamodb_lock_client_options.get_heartbeat_period()
        self.partition_key_name = dynamodb_lock_client_options.get_partition_key_name()
        self.sort_key_name = dynamodb_lock_client_options.get_sort_key_name()
        self.named_thread_creator = dynamodb_lock_client_options.get_named_thread_creator()

        if dynamodb_lock_client_options.get_create_heartbeat_background():
            if self.lease_duration < 2*self.heartbeat_period:
                raise Exception("Heartbeat period must be no more than half the length of the Lease Duration, "
                                + "or locks might expire due to the heartbeat thread taking too long to update them (recommendation is to make it much greater, for example "
                                + "4+ times greater)")
            self.background_thread = self.start_background_thread()
        else:
            self.background_thread = None

    def start_background_thread(self):
        # TODO
        return

    def lock_table_exists(self):
        try:
            self.dynamodb_client.describe_table(TableName=self.table_name)
            return True
        except self.dynamodb_client.exceptions.ResourceNotFoundException:
            return False

    def create_table_in_dynamodb(self, create_dynamodb_table_options):
        create_dynamodb_table_options.create_table()

    def session_monitor_args_validate(self, safe_time_without_heartbeat, heartbeat_period, lease_duration):
        if safe_time_without_heartbeat <= heartbeat_period:
            raise Exception('safeTimeWithoutHeartbeat must be greater than heartbeat frequency')
        elif safe_time_without_heartbeat >= lease_duration:
            raise Exception('safeTimeWithoutHeartbeat must be less than the lock\'s lease duration')

    def acquire_lock(self, acquire_lock_options):
        from .get_lock_options import GetLockOptions
        from ..utils.lock_client_utils import get_time_in_millisecond

        partition_key = acquire_lock_options.get_partition_key()
        sort_key = acquire_lock_options.get_sort_key()

        if self.partition_key_name in acquire_lock_options.keys() \
                or self.OWNER_NAME in acquire_lock_options.keys() \
                or self.LEASE_DURATION in acquire_lock_options.keys() \
                or self.RECORD_VERSION_NUMBER in acquire_lock_options.keys() \
                or self.DATA in acquire_lock_options.keys() \
                or (self.sort_key_name is not None
                    and self.sort_key_name in acquire_lock_options.keys()):
            raise Exception("Additional attribute cannot be one of the following types: " + "%s, %s, %s, %s, %s".format(
                self.partition_key_name, self.OWNER_NAME, self.LEASE_DURATION,
                self.RECORD_VERSION_NUMBER, self.DATA))

        milliseconds_to_wait = self.DEFAULT_BUFFER
        if acquire_lock_options.get_additional_time_to_wait_for_lock() is not None:
            milliseconds_to_wait = acquire_lock_options.get_additional_time_to_wait_for_lock()

        refresh_period_in_milliseconds = self.DEFAULT_BUFFER
        if acquire_lock_options.get_refresh_period() is not None:
            refresh_period_in_milliseconds = acquire_lock_options.get_refresh_period()

        delete_lock_on_release = acquire_lock_options.get_delete_lock_on_release()
        replace_data = acquire_lock_options.get_replace_data()
        session_monitor = acquire_lock_options.get_session_monitor()

        if session_monitor is not None:
            self.session_monitor_args_validate(session_monitor.get_safe_time())

        current_time = get_time_in_millisecond()

        lock_trying_to_be_acquired = None
        already_slept_once_for_one_lease_period = False

        get_lock_options = GetLockOptions.builder(
            partition_key=partition_key).with_sort_key(
            sort_key=sort_key).with_delete_lock_on_release(
            delete_lock_on_release).with_request_metric_collector(
            acquire_lock_options.get_request_metric_collector())

        while True:
            try:
                try:
                    print 'Call GetItem to see if the lock for ' + self.partition_key_name + ' = ' + partition_key + ',' + self.sort_key_name + '=' + sort_key + ' exists in the table'
                    existing_lock = self.get_lock_from_dynamodb(
                        get_lock_options=get_lock_options)

                    new_lock_data = None
                    if replace_data:
                        new_lock_data = acquire_lock_options.get_data()
                    elif existing_lock is not None:
                        new_lock_data = existing_lock.get_data()

                    if new_lock_data is None:
                        new_lock_data = acquire_lock_options.get_data()

                    item = {}
                    item.update(acquire_lock_options.get_additional_attributes())

                    item[self.partition_key_name] = partition_key
                    item[self.OWNER_NAME] = self.owner_name
                    item[self.LEASE_DURATION] = self.lease_duration

                    record_version_number = self.generate_record_version_number()
                    item[self.RECORD_VERSION_NUMBER] = record_version_number
                    if self.sort_key_name is not None:
                        item[self.sort_key_name] = sort_key
                    if new_lock_data is not None:
                        item[self.DATA] = new_lock_data  # TODO: Not sure

                    if existing_lock is None or existing_lock.is_released:
                        return self.upsert_and_monitor_new_or_released_lock(
                            acquire_lock_options, partition_key, sort_key,
                            delete_lock_on_release, session_monitor,
                            new_lock_data, item, record_version_number)

                    if lock_trying_to_be_acquired is None:
                        lock_trying_to_be_acquired = existing_lock

                        if not already_slept_once_for_one_lease_period:
                            already_slept_once_for_one_lease_period = True
                            milliseconds_to_wait += existing_lock.get_lease_duration()

                    else:
                        if lock_trying_to_be_acquired.get_record_revision_number() == existing_lock.get_record_revision_number():
                            if lock_trying_to_be_acquired.is_expired():
                                return self.upsert_and_monitor_expired_lock(
                                    acquire_lock_options, partition_key,
                                    sort_key, delete_lock_on_release,
                                    session_monitor, existing_lock,
                                    new_lock_data, item, record_version_number)
                        else:
                            lock_trying_to_be_acquired = existing_lock
                except Exception as e:
                    print 'Exception:', e
                    pass
            except Exception as e:
                print 'Exception:', e
                pass

            if get_time_in_millisecond() - current_time > milliseconds_to_wait:
                raise Exception('Didn\'t acquire lock after sleeping for more than %d milliseconds', milliseconds_to_wait)
            time.sleep(refresh_period_in_milliseconds/1000.0)

    def get_lock_from_dynamodb(self, get_lock_options):
        result = self.read_from_dynamodb(
            get_lock_options.get_partition_key(),
            get_lock_options.get_sort_key(),
            get_lock_options.get_request_metric_collector())
        if result is None:
            return None
        return self.create_lock_item(get_lock_options, result)

    def read_from_dynamodb(self, partition_key, sort_key, request_metric_collector):

        keys = dict()
        keys[partition_key] = self.partition_key_name
        if sort_key:
            keys[sort_key] = self.sort_key_name

        try:
            response = self.dynamodb_client.get_item(
                TableName=self.table_name,
                ConsistentRead=True,
                Key=keys)
            return response['Item']
        except Exception as e:
            print 'read_from_dynamodb Exception:', e
            return None

    def create_lock_item(self, get_lock_options, dynamodb_item):
        from ..utils.lock_client_utils import get_time_in_millisecond
        from .lock_item import LockItem

        data = dynamodb_item.pop(self.DATA)  # TODO: Not sure about this
        owner_name = dynamodb_item.pop(self.OWNER_NAME)
        lease_duration = dynamodb_item.pop(self.LEASE_DURATION)
        record_revision_number = dynamodb_item.pop(self.RECORD_VERSION_NUMBER)

        if self.IS_RELEASED in dynamodb_item:
            is_released = True
            dynamodb_item.pop(self.IS_RELEASED)
        else:
            is_released = False

        dynamodb_item.pop(self.partition_key_name)

        lookup_time = get_time_in_millisecond()
        lock_item = LockItem(
            dynamodb_lock_client=self,
            partition_key=get_lock_options.get_partition_key(),
            sort_key=get_lock_options.get_sort_key(), data=data,
            owner_name=owner_name,
            delete_lock_item_on_close=get_lock_options.get_delete_lock_on_release(),
            is_released=is_released, lookup_time=lookup_time,
            record_revision_number=record_revision_number,
            lease_duration=lease_duration, additional_attributes=dynamodb_item,
            session_monitor=None)

        return lock_item

    @classmethod
    def generate_record_version_number(cls):
        import uuid
        return str(uuid.uuid4())

    def upsert_and_monitor_new_or_released_lock(
            self, acquire_lock_options, partition_key, sort_key,
            delete_lock_on_release, session_monitor, new_lock_data, item,
            record_version_number):

        expression_attribute_names = {}
        expression_attribute_names[self.PK_PATH_EXPRESSION_VARIABLE] = self.partition_key_name
        expression_attribute_names[self.IS_RELEASED_PATH_EXPRESSION_VARIABLE] = self.IS_RELEASED

        expression_attribute_values = {}
        expression_attribute_values[self.IS_RELEASED_VALUE_EXPRESSION_VARIABLE] = self.IS_RELEASED_ATTRIBUTE_VALUE

        put_item_request = {
            "TableName": self.table_name,
            "Item": item,
            "ConditionExpression": self.ACQUIRE_LOCK_THAT_DOESNT_EXIST_OR_IS_RELEASED_CONDITION,
            "ExpressionAttributeNames": expression_attribute_names,
            "ExpressionAttributeValues": expression_attribute_values
        }

        return self.put_lock_item_and_start_session_monitor(
            acquire_lock_options=acquire_lock_options,
            partition_key=partition_key, sort_key=sort_key,
            delete_lock_on_release=delete_lock_on_release,
            session_monitor=session_monitor, new_lock_data=new_lock_data,
            record_version_number=record_version_number,
            put_item_request=put_item_request)

    def upsert_and_monitor_expired_lock(
            self, acquire_lock_options, partition_key, sort_key,
            delete_lock_on_release, session_monitor, existing_lock,
            new_lock_data, item, record_version_number):

        expression_attribute_values = {}
        expression_attribute_names = {}

        expression_attribute_values[self.RVN_VALUE_EXPRESSION_VARIABLE] = \
            existing_lock.get_recorded_version_number()

        expression_attribute_names[self.PK_PATH_EXPRESSION_VARIABLE] = \
            self.partition_key_name
        expression_attribute_names[self.RVN_PATH_EXPRESSION_VARIABLE] = \
            self.RECORD_VERSION_NUMBER

        if self.sort_key_name is not None:
            conditional_expression = self.PK_EXISTS_AND_SK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION
        else:
            conditional_expression = self.PK_EXISTS_AND_RVN_IS_THE_SAME_CONDITION

        put_item_request = {
            "TableName": self.table_name,
            "Item": item,
            "ConditionExpression": conditional_expression,
            "ExpressionAttributeNames": expression_attribute_names,
            "ExpressionAttributeValues": expression_attribute_values
        }

        print "Acquiring an existing lock whose revisionVersionNumber did not change for " + self.partition_key_name + " partitionKeyName=" + partition_key + ", " + self.sort_key_name + "=" + sort_key
        return self.put_lock_item_and_start_session_monitor(
            acquire_lock_options=acquire_lock_options,
            partition_key=partition_key, sort_key=sort_key,
            delete_lock_on_release=delete_lock_on_release,
            session_monitor=session_monitor, new_lock_data=new_lock_data,
            record_version_number=record_version_number,
            put_item_request=put_item_request)

    def put_lock_item_and_start_session_monitor(
            self, acquire_lock_options, partition_key, sort_key,
            delete_lock_on_release, session_monitor, new_lock_data,
            record_version_number, put_item_request):
        from ..utils.lock_client_utils import get_time_in_millisecond
        from .lock_item import LockItem

        last_updated_time = get_time_in_millisecond()

        self.dynamodb_client.put_item(**put_item_request)

        lock_item = LockItem(
            dynamodb_lock_client=self, partition_key=partition_key,
            sort_key=sort_key, data=new_lock_data, owner_name=self.owner_name,
            delete_lock_item_on_close=delete_lock_on_release,
            is_released=False, lookup_time=last_updated_time,
            record_revision_number=record_version_number,
            lease_duration=self.lease_duration,
            additional_attributes=acquire_lock_options.get_additional_attributes(),
            session_monitor=session_monitor)

        self.locks[lock_item.get_unique_identifier()] = lock_item
        self.try_add_session_monitor(lock_item.get_unique_identifier(), lock_item)
        return lock_item

    def try_add_session_monitor(self, lock_name, lock_item):
        if lock_item.has_session_monitor() and lock_item.has_callback():
            monitor_thread = self.lock_session_monitor_checker(
                lock_name, lock_item)
            monitor_thread.daemon = True
            monitor_thread.start()
            self.session_monitors[lock_name] = monitor_thread

    def lock_session_monitor_checker(self, monitor_name, lock_item):
        # TODO
        raise NotImplementedError

    def release_lock_held_by_user(self, lock_item):
        from .release_lock_options import ReleaseLockOptions
        return self.release_lock(ReleaseLockOptions.builder(
            lock_item=lock_item).with_delete_lock(
            lock_item.get_delete_lock_item_on_close()))

    def release_lock(self, release_lock_options):
        import threading
        lock_item = release_lock_options.lock_item
        delete_lock = release_lock_options.is_delete_lock()
        best_effort = release_lock_options.is_best_effort()
        data = release_lock_options.get_data()

        if lock_item.get_owner_name() != self.owner_name:
            return False

        with threading.Lock(lock_item):  # TODO: Not sure
            self.locks.pop(lock_item.get_unique_identifier())

            expression_attribute_values = {}
            expression_attribute_names = {}

            expression_attribute_values[self.RVN_VALUE_EXPRESSION_VARIABLE] = lock_item.get_recorded_version_number()
            expression_attribute_values[self.OWNER_NAME_VALUE_EXPRESSION_VARIABLE] = lock_item.get_owner_name()

            expression_attribute_names[self.PK_PATH_EXPRESSION_VARIABLE] = self.partition_key_name
            expression_attribute_names[self.OWNER_NAME_PATH_EXPRESSION_VARIABLE] = self.OWNER_NAME
            expression_attribute_names[self.RVN_PATH_EXPRESSION_VARIABLE] = self.RECORD_VERSION_NUMBER
            if self.sort_key_name is not None:
                conditional_expression = self.PK_EXISTS_AND_SK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION
                expression_attribute_names[self.SK_PATH_EXPRESSION_VARIABLE] = self.sort_key_name
            else:
                conditional_expression = self.PK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION

            key = self.get_item_keys(lock_item=lock_item)

            if delete_lock:
                delete_item_request = {
                    "TableName": self.table_name,
                    "Key": key,
                    "ConditionExpression": conditional_expression,
                    "ExpressionAttributeNames": expression_attribute_names,
                    "ExpressionAttributeValues": expression_attribute_values
                }
                self.dynamodb_client.delete_item(**delete_item_request)
            else:
                expression_attribute_names[self.IS_RELEASED_PATH_EXPRESSION_VARIABLE] = self.IS_RELEASED
                expression_attribute_values[self.IS_RELEASED_VALUE_EXPRESSION_VARIABLE] = self.IS_RELEASED_ATTRIBUTE_VALUE

                if data is not None:
                    update_expression = self.UPDATE_IS_RELEASED_AND_DATA
                    expression_attribute_names[self.DATA_PATH_EXPRESSION_VARIABLE] = self.DATA
                    expression_attribute_values[self.DATA_VALUE_EXPRESSION_VARIABLE] = data
                else:
                    update_expression = self.UPDATE_IS_RELEASED
                update_item_request = {
                    "TableName": self.table_name,
                    "Key": key,
                    "UpdateExpression": update_expression,
                    "ConditionExpression": conditional_expression,
                    "ExpressionAttributeNames": expression_attribute_names,
                    "ExpressionAttributeValues": expression_attribute_values
                }
                self.dynamodb_client.update_item(**update_item_request)

            self.remove_kill_session_monitor(lock_item.get_unique_identifier())
        return True

    def get_item_keys(self, lock_item):
        key = dict()
        key[self.partition_key_name] = lock_item.get_partition_key()
        if self.sort_key_name is not None:
            key[self.sort_key_name] = lock_item.get_sort_key()
        return key

    def remove_kill_session_monitor(self, monitor_name):
        if monitor_name in self.session_monitors:
            monitor = self.session_monitors.pop(monitor_name)
            monitor.interrupt()  # TODO: Not sure
            try:
                monitor.join()
            except Exception as e:
                print 'Exception:', e
                pass

    def send_heartbeat(self, send_heartbeat_options):
        import threading
        from ..utils.lock_client_utils import get_time_in_millisecond

        delete_data = send_heartbeat_options.get_delete_data() is not None \
                      and send_heartbeat_options.get_delete_data()
        if delete_data and send_heartbeat_options.get_data() is not None:
            raise Exception('data must not be present if deleteData is true')

        lease_duration_to_ensure = self.lease_duration
        if send_heartbeat_options.get_lease_duration_to_ensure() is not None:
            lease_duration_to_ensure = \
                send_heartbeat_options.get_lease_duration_to_ensure()

        lock_item = send_heartbeat_options.get_lock_item()
        if lock_item.is_expired() \
                or lock_item.get_owner_name() != self.owner_name \
                or lock_item.get_is_released():
            self.locks.pop(lock_item.get_unique_identifier())
            raise Exception('Cannot send heartbeat because lock is not granted')

        with threading.Lock(lock_item):  # TODO: Not sure
            expression_attribute_values = {}
            expression_attribute_names = {}

            expression_attribute_values[self.RVN_VALUE_EXPRESSION_VARIABLE] = lock_item.get_recorded_version_number()
            expression_attribute_values[self.OWNER_NAME_VALUE_EXPRESSION_VARIABLE] = lock_item.get_owner_name()

            expression_attribute_names[self.PK_PATH_EXPRESSION_VARIABLE] = self.partition_key_name
            expression_attribute_names[self.LEASE_DURATION_PATH_VALUE_EXPRESSION_VARIABLE] = self.LEASE_DURATION
            expression_attribute_names[self.RVN_PATH_EXPRESSION_VARIABLE] = self.RECORD_VERSION_NUMBER
            expression_attribute_names[self.OWNER_NAME_PATH_EXPRESSION_VARIABLE] = self.OWNER_NAME

            if self.sort_key_name is not None:
                conditional_expression = self.PK_EXISTS_AND_SK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION
                expression_attribute_names[self.SK_PATH_EXPRESSION_VARIABLE] = self.sort_key_name
            else:
                conditional_expression = self.PK_EXISTS_AND_OWNER_NAME_SAME_AND_RVN_SAME_CONDITION

            record_version_number = self.generate_record_version_number()

            expression_attribute_values[self.NEW_RVN_VALUE_EXPRESSION_VARIABLE] = record_version_number
            expression_attribute_values[self.LEASE_DURATION_VALUE_EXPRESSION_VARIABLE] = lease_duration_to_ensure

            if delete_data:
                expression_attribute_names[self.DATA_PATH_EXPRESSION_VARIABLE] = self.DATA
                update_expression = self.UPDATE_LEASE_DURATION_AND_RVN_AND_REMOVE_DATA
            elif send_heartbeat_options.get_data() is not None:
                expression_attribute_names[self.DATA_PATH_EXPRESSION_VARIABLE] = self.DATA
                expression_attribute_values[self.DATA_VALUE_EXPRESSION_VARIABLE] = send_heartbeat_options.get_data()
                update_expression = self.UPDATE_LEASE_DURATION_AND_RVN_AND_DATA
            else:
                update_expression = self.UPDATE_LEASE_DURATION_AND_RVN

            update_item_request = {
                "TableName": self.table_name,
                "Key": self.get_item_keys(lock_item=lock_item),
                "UpdateExpression": update_expression,
                "ConditionExpression": conditional_expression,
                "ExpressionAttributeNames": expression_attribute_names,
                "ExpressionAttributeValues": expression_attribute_values
            }

            try:
                last_update_of_lock = get_time_in_millisecond()
                self.dynamodb_client.update_item(**update_item_request)
                lock_item.update_record_revision_number(
                    record_version_number=record_version_number,
                    last_update_of_lock=last_update_of_lock,
                    lease_duration_to_ensure=lease_duration_to_ensure)
            except Exception as e:
                print 'Exception:', e
                print 'Someone else acquired the lock, so we will stop heartbeating it'
                self.locks.pop(lock_item.get_unique_identifier())
                raise Exception('Someone else acquired the lock, so we will stop heartbeating it')