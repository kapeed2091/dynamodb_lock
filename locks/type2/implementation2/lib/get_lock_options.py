class GetLockOptions(object):

    def __init__(self, partition_key, sort_key, delete_lock_on_release,
                 request_metric_collector):
        self.partition_key = partition_key
        self.sort_key = sort_key
        self.delete_lock_on_release = delete_lock_on_release
        self.request_metric_collector = request_metric_collector

    def get_partition_key(self):
        return self.partition_key

    def get_sort_key(self):
        return self.sort_key

    def get_delete_lock_on_release(self):
        return self.delete_lock_on_release

    def get_request_metric_collector(self):
        return self.request_metric_collector

    def with_sort_key(self, sort_key):
        self.sort_key = sort_key
        return self

    def with_delete_lock_on_release(self, delete_lock_on_release):
        self.delete_lock_on_release = delete_lock_on_release
        return self

    def with_request_metric_collector(self, request_metric_collector):
        self.request_metric_collector = request_metric_collector
        return self

    @classmethod
    def builder(cls, partition_key):
        return cls(partition_key=partition_key)
