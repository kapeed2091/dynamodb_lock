import boto3
from enum import Enum


class KeyTypeEnum(Enum):
    HASH = 'HASH'
    RANGE = 'RANGE'


class CreateDynamoDBTableOptions(object):

    def __init__(self, table_name, region_name, partition_key_name,
                 sort_key_name, provision_throughput):
        self.table_name = table_name
        self.partition_key_name = partition_key_name
        self.sort_key_name = sort_key_name
        self.provision_throughput = provision_throughput
        self.dynamodb_client = boto3.client('dynamodb',
                                            region_name=region_name)

    def get_table_name(self):
        return self.table_name

    def get_partition_key_name(self):
        return self.partition_key_name

    def get_sort_key_name(self):
        return self.sort_key_name

    def get_provision_throughput(self):
        return self.provision_throughput

    def get_dynamodb_client(self):
        return self.dynamodb_client

    def create_table(self):

        dynamodb_client = self.get_dynamodb_client()
        table_name = self.get_table_name()
        partition_key_name = self.get_partition_key_name()
        sort_key_name = self.get_sort_key_name()
        provision_throughput = self.get_provision_throughput()

        attribute_definitions = [
            {
                'AttributeName': partition_key_name,
                'AttributeType': 'S',
            }
        ]
        keys_schema = [
            {
                'AttributeName': partition_key_name,
                'KeyType': KeyTypeEnum.HASH.value,
            }
        ]

        if sort_key_name is not None:
            attribute_definitions.append({
                'AttributeName': sort_key_name,
                'AttributeType': 'S',
            })
            keys_schema.append({
                'AttributeName': sort_key_name,
                'KeyType': KeyTypeEnum.RANGE.value
            })
            pass

        try:
            dynamodb_client.create_table(
                AttributeDefinitions=attribute_definitions,
                KeySchema=keys_schema,
                ProvisionedThroughput={
                    'ReadCapacityUnits': provision_throughput,
                    'WriteCapacityUnits': provision_throughput,
                },
                TableName=table_name,
            )
        except dynamodb_client.exceptions.ResourceInUseException:
            pass

    def table_exists(self):
        dynamodb_client = self.get_dynamodb_client()
        table_name = self.get_table_name()
        try:
            dynamodb_client.describe_table(TableName=table_name)
            return True
        except dynamodb_client.exceptions.ResourceNotFoundException:
            return False
