class LockItem(object):

    def __init__(self, dynamodb_lock_client, partition_key, sort_key, data,
                 owner_name, delete_lock_item_on_close, is_released,
                 lookup_time, record_revision_number, lease_duration,
                 additional_attributes, session_monitor):
        self.dynamodb_lock_client = dynamodb_lock_client
        self.partition_key = partition_key
        self.sort_key = sort_key
        self.data = data
        self.owner_name = owner_name
        self.delete_lock_item_on_close = delete_lock_item_on_close
        self.is_released = is_released
        self.lookup_time = lookup_time  # in milli seconds
        self.record_revision_number = record_revision_number
        self.lease_duration = lease_duration  # in milli seconds
        self.additional_attributes = additional_attributes
        self.session_monitor = session_monitor

    def get_dynamodb_lock_client(self):
        return self.dynamodb_lock_client

    def get_partition_key(self):
        return self.partition_key

    def get_sort_key(self):
        return self.sort_key

    def get_data(self):
        return self.data

    def get_additional_attributes(self):
        return self.additional_attributes

    def get_owner_name(self):
        return self.owner_name

    def get_lookup_time(self):
        return self.lookup_time

    def get_record_revision_number(self):
        return self.record_revision_number

    def get_lease_duration(self):
        return self.lease_duration

    def get_delete_lock_item_on_close(self):
        return self.delete_lock_item_on_close

    def get_is_released(self):
        return self.is_released

    def get_session_monitor(self):
        return self.session_monitor

    def close(self):
        dynamodb_lock_client = self.get_dynamodb_lock_client()
        return dynamodb_lock_client.release_lock()

    def is_expired(self):
        from ..utils.lock_client_utils import get_time_in_millisecond
        if self.is_released:
            return True
        lookup_time = self.get_lookup_time()
        lease_duration = self.get_lease_duration()
        return get_time_in_millisecond() - lookup_time > lease_duration

    def send_heartbeat(self):
        dynamodb_lock_client = self.get_dynamodb_lock_client()
        dynamodb_lock_client.send_heartbeat()

    def ensure(self, lease_duration_to_ensure, time_unit):
        from ..utils.lock_client_utils import get_time_in_millisecond
        from .send_heartbeat_options import SendHeartbeatOptions
        dynamodb_lock_client = self.get_dynamodb_lock_client()
        is_released = self.get_is_released()
        lease_duration = self.get_lease_duration()
        lookup_time = self.get_lookup_time()
        if is_released:
            raise Exception('Lock is released')
        if (lease_duration - (get_time_in_millisecond() - lookup_time)
                <= lease_duration_to_ensure):
            dynamodb_lock_client.send_heartbeat(
                SendHeartbeatOptions(self).with_lease_duration_to_ensure(
                    lease_duration_to_ensure).with_time_unit(time_unit))
        pass

    def update_record_revision_number(
            self, record_version_number, last_update_of_lock,
            lease_duration_to_ensure):
        self.record_revision_number = record_version_number
        self.lookup_time = last_update_of_lock
        self.lease_duration = lease_duration_to_ensure
        pass

    def get_unique_identifier(self):
        partition_key = self.partition_key
        sort_key = self.sort_key
        if sort_key is None:
            sort_key = ""
        return partition_key + sort_key

    def am_i_about_to_expire(self):
        return self.milliseconds_until_danger_zone_entered() <= 0

    def milliseconds_until_danger_zone_entered(self):
        session_monitor = self.get_session_monitor()
        is_released  = self.get_is_released()
        lookup_time = self.get_lookup_time()
        if not session_monitor.is_present():
            raise Exception('SessionMonitor is not set')
        if is_released:
            raise Exception('Lock is already released')
        return session_monitor.milliseconds_until_lease_enters_danger_zone(
            lookup_time)

    def has_session_monitor(self):
        session_monitor = self.get_session_monitor()
        return session_monitor.is_present()

    def has_callback(self):
        session_monitor = self.get_session_monitor()
        if not session_monitor.is_present():
            raise Exception('SessionMonitor is not set')
        return session_monitor.has_callback()

    def run_session_monitor(self):
        session_monitor = self.get_session_monitor()
        if not session_monitor.is_present():
            raise Exception('Can\'t run callback without first setting SessionMonitor')
        session_monitor.has_callback()
