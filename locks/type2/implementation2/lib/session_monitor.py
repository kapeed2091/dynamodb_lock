class SessionMonitor(object):
    def __init__(self, safe_time_without_heartbeat, callback):
        self.safe_time_without_heartbeat = safe_time_without_heartbeat
        self.callback = callback

    def get_safe_time_without_heartbeat(self):
        return self.safe_time_without_heartbeat

    def get_callback(self):
        return self.callback

    def milliseconds_until_lease_enters_danger_zone(
            self, last_absolute_time_updated):
        from ..utils.lock_client_utils import get_time_in_millisecond
        safe_time_without_heartbeat = self.get_safe_time_without_heartbeat()
        return last_absolute_time_updated + safe_time_without_heartbeat - get_time_in_millisecond()

    def is_lease_entering_danger_zone(self, last_absolute_time_updated):
        return self.milliseconds_until_lease_enters_danger_zone(last_absolute_time_updated) <= 0

    def run_callback(self):
        callback = self.get_callback()
        if callback.is_present():
            # TODO:
            """
            final Thread t = new Thread(this.callback.get());
            t.setDaemon(true);
            t.start();
            """
            raise NotImplementedError

    def has_callback(self):
        callback = self.get_callback()
        return callback != None

    def get_safe_time(self):
        return self.get_safe_time_without_heartbeat()
