class SendHeartbeatOptions(object):

    def __init__(self, lock_item, data=None, delete_data=None,
                 lease_duration_to_ensure=None, time_unit=None,
                 request_metric_collector=None):
        self.lock_item = lock_item
        self.data = data
        self.delete_data = delete_data
        self.lease_duration_to_ensure = lease_duration_to_ensure
        self.time_unit = time_unit
        self.request_metric_collector = request_metric_collector

    def get_lock_item(self):
        return self.lock_item

    def get_data(self):
        return self.data

    def get_delete_data(self):
        return self.delete_data

    def get_lease_duration_to_ensure(self):
        return self.lease_duration_to_ensure

    def get_time_unit(self):
        return self.time_unit

    def get_request_metric_collector(self):
        return self.request_metric_collector

    def with_data(self, data):
        self.data = data
        return self

    def with_delete_data(self, delete_data):
        self.delete_data = delete_data
        return self

    def with_lease_duration_to_ensure(self, lease_duration_to_ensure):
        self.lease_duration_to_ensure = lease_duration_to_ensure
        return self

    def with_time_unit(self, time_unit):
        self.time_unit = time_unit
        return self

    def with_request_metric_collector(self, request_metric_collector):
        self.request_metric_collector = request_metric_collector
        return self

    @classmethod
    def builder(cls, lock_item):
        return cls(lock_item=lock_item)
