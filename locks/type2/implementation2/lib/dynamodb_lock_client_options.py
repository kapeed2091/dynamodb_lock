import uuid
from enum import Enum


class TimeUnitEnum(Enum):
    SECOND = 'SECOND'


class DynamoDBLockClientOptionsBuilder(object):
    DEFAULT_REGION_NAME = 'ap-south-1'
    DEFAULT_PARTITION_KEY_NAME = 'key'
    DEFAULT_SORT_KEY_NAME = None
    DEFAULT_LEASE_DURATION = 20
    DEFAULT_HEARTBEAT_PERIOD = 5
    DEFAULT_TIME_UNIT = TimeUnitEnum.SECOND.value
    DEFAULT_PROVISION_THROUGHPUT = 5
    DEFAULT_CREATE_HEARTBEAT_BACKGROUND_THREAD = True

    def __init__(self, dynamodb_client, table_name,
                 region_name=DEFAULT_REGION_NAME,
                 partition_key_name=DEFAULT_PARTITION_KEY_NAME,
                 sort_key_name=DEFAULT_SORT_KEY_NAME,
                 provision_throughput=DEFAULT_PROVISION_THROUGHPUT,
                 lease_duration=DEFAULT_LEASE_DURATION,
                 heartbeat_period=DEFAULT_HEARTBEAT_PERIOD,
                 time_unit=DEFAULT_TIME_UNIT,
                 create_heartbeat_background_thread=DEFAULT_CREATE_HEARTBEAT_BACKGROUND_THREAD):
        self.dynamodb_client = dynamodb_client
        self.table_name = table_name
        self.region_name = region_name
        self.partition_key_name = partition_key_name
        self.sort_key_name = sort_key_name
        self.provision_throughput = provision_throughput
        self.lease_duration = lease_duration
        self.heartbeat_period = heartbeat_period
        self.time_unit = time_unit
        self.create_heartbeat_background = create_heartbeat_background_thread
        self.owner_name = self.generate_owner_name()

    @classmethod
    def generate_owner_name(cls):
        return str(uuid.uuid4())

    def get_dynamodb_client(self):
        return self.dynamodb_client

    def get_table_name(self):
        return self.table_name

    def get_region_name(self):
        return self.region_name

    def get_partition_key_name(self):
        return self.partition_key_name

    def get_sort_key_name(self):
        return self.sort_key_name

    def get_provision_throughput(self):
        return self.provision_throughput

    def get_lease_duration(self):
        return self.lease_duration

    def get_heartbeat_period(self):
        return self.heartbeat_period

    def get_time_unit(self):
        return self.time_unit

    def get_create_heartbeat_background(self):
        return self.create_heartbeat_background

    def get_owner_name(self):
        return self.owner_name

    def get_named_thread_creator(self):
        return self.named_thread_creator

    def named_thread_creator(self):
        import threading
        return threading.Thread()

    def with_partition_key_name(self, partition_key_name):
        self.partition_key_name = partition_key_name
        return self

    def with_sort_key_name(self, sort_key_name):
        self.sort_key_name = sort_key_name
        return self

    def with_owner_name(self, owner_name):
        self.owner_name = owner_name
        return self

    def with_lease_duration(self, lease_duration):
        self.lease_duration = lease_duration
        return self

    def with_heartbeat_period(self, heartbeat_period):
        self.heartbeat_period = heartbeat_period
        return self

    def with_time_unit(self, time_unit):
        self.time_unit = time_unit
        return self

    def with_create_heartbeat_background(self, create_heartbeat_background):
        self.create_heartbeat_background = create_heartbeat_background
        return self

    @classmethod
    def builder(cls, dynamodb_client, table_name):
        return cls(dynamodb_client=dynamodb_client, table_name=table_name)

    # def builder(self):
    #     from .create_dynamodb_table_options import CreateDynamoDBTableOptions
    #
    #     table_name = self.get_table_name()
    #     region_name = self.get_region_name()
    #     partition_key_name = self.get_partition_key_name()
    #     sort_key_name = self.get_sort_key_name()
    #     provision_throughput = self.get_provision_throughput()
    #
    #     create_dynamodb_table_options = CreateDynamoDBTableOptions(
    #         table_name=table_name, region_name=region_name,
    #         partition_key_name=partition_key_name, sort_key_name=sort_key_name,
    #         provision_throughput=provision_throughput)
    #
    #     create_dynamodb_table_options.create_table()
