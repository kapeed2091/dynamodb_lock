class AcquireLockOptions(object):

    def __init__(
            self, partition_key, sort_key=None, data=None, replace_data=None,
            delete_lock_on_release=None, refresh_period=None,
            additional_time_to_wait_for_lock=None, time_unit=None,
            additional_attributes=None, session_monitor=None,
            request_metric_collector=None, safe_time_without_heartbeat=None,
            session_monitor_callback=None, is_session_monitor_set=False):
        self.partition_key = partition_key
        self.sort_key = sort_key
        self.data = data
        self.replace_data = replace_data
        self.delete_lock_on_release = delete_lock_on_release
        self.refresh_period = refresh_period
        self.additional_time_to_wait_for_lock = additional_time_to_wait_for_lock
        self.time_unit = time_unit
        self.additional_attributes = additional_attributes
        self.session_monitor = session_monitor
        self.request_metric_collector = request_metric_collector
        self.safe_time_without_heartbeat = safe_time_without_heartbeat
        self.session_monitor_callback = session_monitor_callback
        self.is_session_monitor_set = is_session_monitor_set

    def get_partition_key(self):
        return self.partition_key

    def get_sort_key(self):
        return self.sort_key

    def get_data(self):
        return self.data

    def get_replace_data(self):
        return self.replace_data

    def get_delete_lock_on_release(self):
        return self.delete_lock_on_release

    def get_refresh_period(self):
        return self.refresh_period

    def get_additional_time_to_wait_for_lock(self):
        return self.additional_time_to_wait_for_lock

    def get_time_unit(self):
        return self.time_unit

    def get_additional_attributes(self):
        return self.additional_attributes

    def get_session_monitor(self):
        return self.session_monitor

    def get_request_metric_collector(self):
        return self.request_metric_collector

    def get_safe_time_without_heartbeat(self):
        return self.safe_time_without_heartbeat

    def get_session_monitor_callback(self):
        return self.session_monitor_callback

    def get_is_session_monitor_set(self):
        return self.is_session_monitor_set

    def with_sort_key(self, sort_key):
        self.sort_key = sort_key
        return self

    def with_data(self, data):
        self.data = data
        return self

    def with_replace_data(self, replace_data):
        self.replace_data = replace_data
        return self

    def with_delete_lock_on_release(self, delete_lock_on_release):
        self.delete_lock_on_release = delete_lock_on_release
        return self

    def with_refresh_period(self, refresh_period):
        self.refresh_period = refresh_period
        return self

    def with_additional_time_to_wait_for_lock(self, additional_time_to_wait_for_lock):
        self.additional_time_to_wait_for_lock = additional_time_to_wait_for_lock
        return self

    def with_time_unit(self, time_unit):
        self.time_unit = time_unit
        return self

    def with_additional_attributes(self, additional_attributes):
        self.additional_attributes = additional_attributes
        return self

    def with_session_monitor(self, safe_time_without_heartbeat, session_monitor_callback):
        self.safe_time_without_heartbeat = safe_time_without_heartbeat
        self.session_monitor_callback = session_monitor_callback
        self.is_session_monitor_set = True
        return self

    def with_request_metric_collector(self, request_metric_collector):
        self.request_metric_collector = request_metric_collector
        return self

    def build(self):
        from .session_monitor import SessionMonitor
        safe_time_without_heartbeat = self.get_safe_time_without_heartbeat()
        is_session_monitor_set = self.get_is_session_monitor_set()
        session_monitor_callback = self.get_session_monitor_callback()
        if is_session_monitor_set:
            session_monitor = SessionMonitor(safe_time_without_heartbeat, session_monitor_callback)
        else:
            session_monitor = None

        partition_key = self.get_partition_key()
        sort_key = self.get_sort_key()
        data = self.get_data()
        replace_data = self.get_replace_data()
        delete_lock_on_release = self.get_delete_lock_on_release()
        refresh_period = self.get_refresh_period()
        additional_time_to_wait_for_lock = self.get_additional_time_to_wait_for_lock()
        time_unit = self.get_time_unit()
        additional_attributes = self.get_additional_attributes()
        request_metric_collector = self.get_request_metric_collector()

        return AcquireLockOptions(
            partition_key=partition_key, sort_key=sort_key, data=data,
            replace_data=replace_data, delete_lock_on_release=delete_lock_on_release,
            refresh_period=refresh_period, additional_time_to_wait_for_lock=additional_time_to_wait_for_lock,
            time_unit=time_unit, additional_attributes=additional_attributes,
            session_monitor=session_monitor, request_metric_collector=request_metric_collector
        )

    def equals(self, other):
        if other is None or not isinstance(other, AcquireLockOptions):
            return False

        return self.partition_key == other.partition_key \
               and self.sort_key == other.sort_key \
               and self.data == other.data \
               and self.replace_data == other.replace_data \
               and self.delete_lock_on_release == other.delete_lock_on_release\
               and self.refresh_period == other.refresh_period\
               and self.additional_time_to_wait_for_lock == other.additional_time_to_wait_for_lock \
               and self.time_unit == other.time_unit \
               and self.additional_attributes == other.additional_attributes \
               and self.session_monitor == other.session_monitor \
               and self.request_metric_collector == other.request_metric_collector

    @classmethod
    def builder(cls, partition_key):
        return cls(partition_key=partition_key)
