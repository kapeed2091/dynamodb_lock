import boto3
import time
import uuid


class DynamoDBLock(object):

    def __init__(self, lock_db_name, region_name='ap-south-1'):
        self.lock_db_name = lock_db_name
        self.dynamodb_client = boto3.client('dynamodb',
                                            region_name=region_name)
        self.region_name = region_name

    def create_table(self):
        try:
            self.dynamodb_client.create_table(
                AttributeDefinitions=[
                    {
                        'AttributeName': 'name',
                        'AttributeType': 'S',
                    }
                ],
                KeySchema=[
                    {
                        'AttributeName': 'name',
                        'KeyType': 'HASH',
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5,
                },
                TableName=self.lock_db_name,
            )
        except self.dynamodb_client.exceptions.ResourceInUseException:
            print 'Table exists'
            pass

    def acquire_lock(self, lock_name, lock_duration_in_milliseconds,
                     existing_luid=None):
        """

        :param lock_name:
        :param lock_duration_in_milliseconds:
        :param existing_luid:
        :return:

        """
        new_luid = str(uuid.uuid4())
        time_now = time.time()
        expires_at = time_now + lock_duration_in_milliseconds

        params = {
            "TableName": self.lock_db_name,
            "Item": {
                "name": {
                    "S": lock_name,
                },
                "expires_in_milliseconds": {
                    "N": str(lock_duration_in_milliseconds)
                },
                "luid": {
                    "S": new_luid
                },
                "expires_at": {
                    "S": str(expires_at)
                }
            }
        }

        if existing_luid is None:
            condition_expression = "attribute_not_exists(luid)"
            params.update({"ConditionExpression": condition_expression})
        else:
            condition_expression = "attribute_not_exists(luid) OR " \
                                   "(attribute_exists(luid) AND luid = :luid)"
            params.update({"ConditionExpression": condition_expression})

            expression_attribute_values = {
                ":luid": {"S": existing_luid}
            }
            params.update({"ExpressionAttributeValues": expression_attribute_values})


        try:
            self.dynamodb_client.put_item(**params)
            acquire_lock = True
        except Exception as e:
            print 'Exception:', e
            acquire_lock = False

        return acquire_lock

    def release_lock(self, lock_name):
        # TODO: Should we compare luid as well?
        params = {
            "TableName": self.lock_db_name,
            "Key": {
                "name": {
                    "S": lock_name,
                }
            }
        }
        try:
            self.dynamodb_client.delete_item(**params)
        except Exception as e:
            print 'Exception:', e
            pass
        return

    def reacquire_lock(self, lock_name, lock_duration_in_milliseconds,
                       existing_luid):
        time_now = time.time()
        expires_at = time_now + lock_duration_in_milliseconds
        params = {
            "TableName": self.lock_db_name,
            "Key": {
                "name": {
                    "S": lock_name,
                }
            },
            "UpdateExpression": "SET expires_in_milliseconds = :expires_in_milliseconds, expires_at = :expires_at",
            "ConditionExpression": "attribute_exists(luid) AND luid = :luid",
            "ExpressionAttributeValues": {
                ":expires_in_milliseconds": {"N": str(lock_duration_in_milliseconds)},
                ":expires_at": {"N": str(expires_at)},
                ":luid": {"S": existing_luid}
            }
        }

        try:
            self.dynamodb_client.update_item(**params)
            return True
        except Exception as e:
            print 'Exception:', e
            return False

    def acquire_lock_by_sleeping(self, lock_name, lock_duration_in_milliseconds,
                                heartbeat_period_in_milliseconds,
                                existing_luid):
        params = {
            "TableName": self.lock_db_name,
            "Key": {
                "name": {
                    "S": lock_name,
                }
            }
        }
        response = self.dynamodb_client.get_item(**params)

        dynamodb_item = response['Item']
        expires_in_milliseconds = \
            int(dynamodb_item['expires_in_milliseconds']['N'])

        last_updated_time = get_time_in_milliseconds()

        while True:
            lock_status = self.acquire_lock(
                lock_name=lock_name,
                lock_duration_in_milliseconds=lock_duration_in_milliseconds,
                existing_luid=existing_luid)

            if lock_status:
                # Lock acquired
                return True

            if get_time_in_milliseconds() - last_updated_time > expires_in_milliseconds:
                # Slept for more than expired time
                break

            time.sleep(heartbeat_period_in_milliseconds/1000.0)

        # Acquiring lock with latest luid as waited for more than 'expires_in'
        lock_status = self.acquire_lock(
            lock_name=lock_name,
            lock_duration_in_milliseconds=lock_duration_in_milliseconds,
            existing_luid=dynamodb_item['luid']['S'])
        return lock_status

def get_time_in_milliseconds():
    return time.time() * 1000